FROM openjdk:8
ARG JAR_FILE=./api-autotest-backend/application/target/application-1.0-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]